<h1>Conseguir Trabajo como Frontend Developer</h1>

<h3>Estefany Aguilar</h3>

<h1>Table of Content</h1>

- [1. Conoce tu perfil profesional](#1-conoce-tu-perfil-profesional)
  - [¿Qué necesitas para convertirte en frontend developer?](#qué-necesitas-para-convertirte-en-frontend-developer)
  - [¿Cuál es la diferencia entre junior, semisenior y senior?](#cuál-es-la-diferencia-entre-junior-semisenior-y-senior)
  - [Tips y herramientas para crear tu curriculum vitae](#tips-y-herramientas-para-crear-tu-curriculum-vitae)
  - [Ideas para crear tu página web](#ideas-para-crear-tu-página-web)
  - [Preguntas y respuestas sobre tu perfil profesional](#preguntas-y-respuestas-sobre-tu-perfil-profesional)
- [2. Preparación previa a presentar entrevistas técnicas](#2-preparación-previa-a-presentar-entrevistas-técnicas)
  - [Prepárate en HTML y CSS (nivel teórico y práctico)](#prepárate-en-html-y-css-nivel-teórico-y-práctico)
  - [JavaScript a nivel teórico y práctico](#javascript-a-nivel-teórico-y-práctico)
  - [Frameworks de JS a nivel teórico y práctico](#frameworks-de-js-a-nivel-teórico-y-práctico)
  - [Resolución de problemas y live coding](#resolución-de-problemas-y-live-coding)
  - [3 ejercicios para poner en práctica tus habilidades en HTML, CSS y JS.](#3-ejercicios-para-poner-en-práctica-tus-habilidades-en-html-css-y-js)
- [3. Cómo presentar entrevistas técnicas](#3-cómo-presentar-entrevistas-técnicas)
  - [Recomendaciones generales para una entrevista técnica](#recomendaciones-generales-para-una-entrevista-técnica)
  - [Buenas prácticas de HTML, CSS y JavaScript para una entrevista](#buenas-prácticas-de-html-css-y-javascript-para-una-entrevista)
  - [HTML, CSS y JavaScript a nivel práctico para entrevistas](#html-css-y-javascript-a-nivel-práctico-para-entrevistas)
  - [Mejores prácticas para presentar entrevistas técnicas](#mejores-prácticas-para-presentar-entrevistas-técnicas)
- [4. Cómo resolver pruebas técnicas en casa](#4-cómo-resolver-pruebas-técnicas-en-casa)
  - [Entendimiento general del problema](#entendimiento-general-del-problema)
  - [Elección del framework para realizar la prueba](#elección-del-framework-para-realizar-la-prueba)
  - [Cómo ejecutar una prueba técnica](#cómo-ejecutar-una-prueba-técnica)
  - [Preguntas y respuestas sobre pruebas técnicas en casa](#preguntas-y-respuestas-sobre-pruebas-técnicas-en-casa)
- [5. Experiencias, historias y traumas personales](#5-experiencias-historias-y-traumas-personales)
  - [Cómo estar al día en tecnología: comunidades, conferencias, blogs y más](#cómo-estar-al-día-en-tecnología-comunidades-conferencias-blogs-y-más)
  - [Proyectos individuales y contribución a proyectos open source](#proyectos-individuales-y-contribución-a-proyectos-open-source)
  - [Mi experiencia en entrevistas: lo que hubiera hecho (y lo que no)](#mi-experiencia-en-entrevistas-lo-que-hubiera-hecho-y-lo-que-no)
  - [Preguntas y respuestas sobre cómo estar al día](#preguntas-y-respuestas-sobre-cómo-estar-al-día)

# 1. Conoce tu perfil profesional

## ¿Qué necesitas para convertirte en frontend developer?

**Responsabilidades como frontend**

- Pasar del diseño a la web
- Asegurar la compatibilidad y que sea responsive
- Crear codigo reutilizable
- Optimizar sitios web (rendimiento, velocidad y escalables)
- Hacer sitios web accesibles

**Que necesito para cumplir con las responsabilidades**

1. Tener buenas bases de HTML, CSS y Javascript
2. Conocer frameworks de javascript (ReactJS, Angular, VueJS)
3. Conocer algun framework de CSS (bootstrap, materialize, tailwindcss)
4. Conocer un preprocesador (SASS, Stylus)
5. Consumo de API’s
6. Saber diseño responsive
7. Hacer sitios compatibles para todos los navegadores (chrome, firefox, safari)
8. Conocer sobre testing y debuggin
9. Saber algun tipo de versionamiento para trabajo colaborativo (Git, SVN)
10. Saber de accesibilidad, performance y rendimiento

[![img](https://www.google.com/s2/favicons?domain=https://skillcrush.com/wp-content/themes/skillcrush-corgi/img/favicons/apple-touch-icon.png)13 Skills You Need to Become a Front End Developer in 2021 - Skillcrush](https://skillcrush.com/blog/skills-to-become-a-front-end-developer/)

## ¿Cuál es la diferencia entre junior, semisenior y senior?

según su experiencia y habilidades:

![Junior](https://farm8.staticflickr.com/7926/47232088682_6150d2cfaa_o.jpg)

![Semi-senior](https://farm8.staticflickr.com/7904/47232081282_41ba0f44ec_o.jpg)

![Senior](https://farm8.staticflickr.com/7927/46369587695_de84526f14_o.jpg)

**Junior**
Personas que apenas están comenzando, apasionados por el conocimiento

- Menos de 2 años de experiencia
- Necesita ser guiado
- Tiene muchas ganas de aprender

**Semi Senior**

- Entre 2 y 5 ó 6 años de experiencia
- No requiere constante guianza por parte del equipo
- Es proactivo
- Es propositivo

**Senior**

- Más de 5 ó 6 años de experiencia
- Alta autonomía, no requiere ser guiado
- Alta productividad
- Toma decisiones basadas en experiencias y es responsable de sus resultados
- Entiende cómo puede causar impacto a sus usuarios
- Sabe tanto de software como del negocio
- Puede convertir a otras personas en senior devs

## Tips y herramientas para crear tu curriculum vitae

**Secciones relevantes para el CV**

1. Nombre y rol
2. Datos de contacto (correo, linkedin, github)
3. Breve descripcion de uno mismo
4. Habilidades (especificar, tecnologias)
5. Experiencia (Cargo, tiempo, empresa, descripcion de lo que hiciste)
6. Educacion (universidad, cursos relevantes)
7. Referencias (personales y laborales)

**Tips para crear tu CV**

1. Que hable de ti
2. Que tenga secciones relevantes (2 pág máx)
3. Que tenga un objetivo => Conseguir tu primera entrevista

**Secciones relevantes para el CV**

- Nombre y rol. Que sea claro y visible y que se pueda ver bien quien eres y cual es tu rol
- Datos de contacto. Correo, telefono, redes sociales, github, linkedin etc.
- Resumen. Breve descripción sobre ti, las cosas que te apasionan, lo que estas estudiando etc
- Habilidades. Ejemplo: Git, Javascript, React etc.
- Experiencia profesional. Que diga el cargo y título que tuviste, por ejemplo: Software Developer. También que diga el tiempo que duraste en este rol y el nombre de la compañía.
  Acompañalo con un breve párrafo descriptivo que hable sobre lo que hiciste en ese cargo
- Educación. Universidades y certificaciones de cursos más relevantes
- Referencias

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Attention Required! | Cloudflare](https://www.canva.com/learn/resume-format/)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)Attention Required! | Cloudflare](https://www.canva.com/resumes/templates/modern/)

[Build a job](https://flowcv.io/)

[<img src="https://brandonpalmeros.me/assets/profile.png" alt="img" style="zoom:25%;" />](https://brandonpalmeros.me/)

## Ideas para crear tu página web

Este es tu momento para desarrollar toda la creatividad que tienes.

Vamos a demostrar en qué somos buenos y que nos gusta ó nos gustaría ser

- Hazla en el lenguaje o framework que quieres aprender

**Qué debe tener tu página web**

- Nombre
- Tiene que hablar de ti. que se pueda visualizar claramente cual es tu estilo
- Contacto. Redes sociales, email etc
- Proyectos. Trabajos que has realizado
- Tus skills o habilidades técnicas
- Si eres creador/creadora de contenido también puedes incluir un blog

[![img](https://www.google.com/s2/favicons?domain=https://colorlib.com/wp/wp-content/uploads/sites/2/2014/05/colorlib-favicon.png)25 Best HTML5 Resume Templates For Personal Portfolios 2021 - Colorlib](https://colorlib.com/wp/html5-resume-templates/)

## Preguntas y respuestas sobre tu perfil profesional

- En nuestra hoja de vida / cv no tenemos que poner nuestra experiencia laboral que no este relacionada a trabajos del mundo IT.
- Si es viable entrar al mundo de la programación por más que tengamos una edad avanzada
- No utilizar barras de porcentaje en el cv/portafolio ya que es subjetivo.
- Si no tenemos experiencia laboral, lo que podemos hacer es crear proyectos personales y publicarlos, también otra opción es buscar pequeños trabajos como freelance.

# 2. Preparación previa a presentar entrevistas técnicas

## Prepárate en HTML y CSS (nivel teórico y práctico)

### **A nivel práctico**

- Aprender las bases. Trata de entender cada cosa
- Crea pequeños componentes. ejemplo: un navbar
- Imaginar diferentes escenarios.
- Replica páginas web existentes

### **A nivel teórico**

En HTML debes conocer:

- sintaxis
- tipo de etiquetas
- HTML semántico
- performance
- DOM

**En CSS debes conocer:**

- Modelo de caja
- Tipos de display
- Pseudo-clases
- Media queries
- Arquitectura
- Versiones
- Especificidad

[![img](https://www.google.com/s2/favicons?domain=https://material.io/design/static/assets/favicon.ico)Material Design](https://material.io/design)

[![img](https://www.google.com/s2/favicons?domain=https://getbootstrap.com//docs/5.1/assets/img/favicons/apple-touch-icon.png)Bootstrap · The most popular HTML, CSS, and JS library in the world.](https://getbootstrap.com/)

[![img](https://www.google.com/s2/favicons?domain=https://www.uistore.design/images/favicon.png)Components Free UI Kit - uistore.design](https://www.uistore.design/items/components-free-ui-kit/)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.dribbble.com/assets/dribbble-vector-ball-f320a6ba48a4ccf416ef6e396e204c899874565b694593834b6c23f978394498.svg)😇 Dashboard Card - UI Components by © Zaini Achmad 🦁 for Vektora on Dribbble](https://dribbble.com/shots/14924187--Dashboard-Card-UI-Components)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.dribbble.com/assets/dribbble-vector-ball-f320a6ba48a4ccf416ef6e396e204c899874565b694593834b6c23f978394498.svg)travel ui kit by Nur Asyrof Muhammad on Dribbble](https://dribbble.com/shots/14411871-travel-ui-kit)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.dribbble.com/assets/dribbble-vector-ball-f320a6ba48a4ccf416ef6e396e204c899874565b694593834b6c23f978394498.svg)Figma UI Kits - Dark Mode by Brandi on Dribbble](https://dribbble.com/shots/12876242-Figma-UI-Kits-Dark-Mode)

[![img](https://www.google.com/s2/favicons?domain=https://static.platzi.com/media/favicons/platzi_favicon.png)https://typegang.com/inspiration/ui/lifestyle-blog-design/](https://typegang.com/inspiration/ui/lifestyle-blog-design/)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.dribbble.com/assets/dribbble-vector-ball-f320a6ba48a4ccf416ef6e396e204c899874565b694593834b6c23f978394498.svg)Style exploration / Plants care app by Anass Drissi on Dribbble](https://dribbble.com/shots/8578893-Style-exploration-Plants-care-app)

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)44 etiquetas HTML que debes conocer](https://platzi.com/blog/etiquetas-html-debes-conocer)

## JavaScript a nivel teórico y práctico

### **A nivel teórico**

- Aprende las bases
- No te abrumes con Javascript. Sigue practicando
- Ten paciencia
- Arma un plan. Organízate
- [33 conceptos de Javascript que debes conocer](https://github.com/leonardomso/33-js-concepts)

### **A nivel práctico**

Haz mini proyectos:

- Consumo de APIs
- Replica funcionalidades de apps que te gusten
- Crea tu propio proyecto. Ej. Una página web

### **Cuando hagas un curso**

1. Se espectador
2. Trata de recordar
3. Replica sin ver

**Javascript es de tiempo y experiencia. **

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - leonardomso/33-js-concepts: 📜 33 JavaScript concepts every developer should know.](https://github.com/leonardomso/33-js-concepts)

## Frameworks de JS a nivel teórico y práctico

>  NO comiences con ningún framework si no tienes las bases de Javascript claras

### **A nivel teórico**

- Haz un curso donde aprendas las bases
  **Cuando hagas un curso…**

1. Se espectador
2. Trata de recordar
3. Replica sin ver

### **A nivel práctico**

Practica con baby steps

- Fundamentos
- Componentes
- Consumo de APIs
- Enrutamiento



[![img](https://www.google.com/s2/favicons?domain=https://cdn.dribbble.com/assets/dribbble-vector-ball-f320a6ba48a4ccf416ef6e396e204c899874565b694593834b6c23f978394498.svg)Food Delivery service - App Design by Anastasia on Dribbble](https://dribbble.com/shots/14803442-Food-Delivery-service-App-Design)

[![img](https://www.google.com/s2/favicons?domain=https://www.ideamotive.co/hubfs/im_favicon_48x48_01.png)21 Dazzling Examples of Mobile App UI Design to Inspire You in 2021](https://www.ideamotive.co/blog/dazzling-examples-of-mobile-app-ui-design)

[![img](https://www.google.com/s2/favicons?domain=https://cdn.dribbble.com/assets/dribbble-vector-ball-f320a6ba48a4ccf416ef6e396e204c899874565b694593834b6c23f978394498.svg)Starbuck by MindInventory UI/UX on Dribbble](https://dribbble.com/shots/7011781-Starbuck)

## Resolución de problemas y live coding

### **Live coding**

1. Muestra que piensas en el problema y analiza la situación
2. Piensa en voz alta. Te puede ayudar para que el entrevistador te de una mano si nota que estás yendo por otro camino
3. No te des por vencido.**Busca soluciones**.¿Cómo te enfrentas a problemas? Si no logras resolver el ejercicio la idea es que puedas demostrar que sabes cómo manejar esas situaciones difíciles. Aprender a manejar la frustración, trabajar en soft skills

### **Resolución de problemas**

Resolver problemas con codigo. Ejemplos de problemas:

- Cuántas veces se repite una palabra en una frase?
- Elimina los datos repetido de un array
- Mirar si hay dos números que sumen un número determinado
- Saber si una palabra es palíndromo
- Invertir una frase

### **Adicional**

- Manipulación del DOM
- Mostrar info de un JSON
- Slider
- Petición a API público. Ej. Giphy
- form que guarde local storage

[<img src="https://cdn3.iconfinder.com/data/icons/inficons/512/github.png" alt="Github icon - Free download on Iconfinder" style="zoom:5%;" />**public-apis/public-apis**](https://github.com/public-apis/public-apis)

## 3 ejercicios para poner en práctica tus habilidades en HTML, CSS y JS.

3 ejercicios para poner en práctica tus habilidades en HTML, CSS y JS.
Módulo: Prepárate para presentar entrevistas técnicas.

Para finalizar nuestro módulo, te compartiré 3 ejercicios para que pongas en práctica tus conocimientos en HTML, CSS y JS.

## Buscador de GIF

Esta página debe contener una barra de búsqueda y una grid principal (con 3 columnas en desktop, 2 en tablet y 1 en mobile) que muestre GIF aleatorios. Una vez se digite 3 letras en la barra de búsqueda, se empiezan a mostrar GIF relacionados con la búsqueda en la grid principal.

Para este ejercicio, usa la [API de GIPHY](https://developers.giphy.com/). Puedes tomar este diseño como referencia:

![gif](https://developers.giphy.com/branch/master/static/waterfall_dark-ab4a891d1961307879417cbb3aeff6cd.gif)

## TODO List de emojis

Esta página debe contener una barra de búsqueda que recibirá como entrada las actividades de tu rutina diaria como: “comer”, “correr”, “pasear mascota”, “dormir”, entre otras; y un botón de “agregar” que se encargará de mostrar debajo de la barra de búsqueda un emoji correspondiente a la actividad ingresada.

Si se ingresa más de una actividad, deben mostrarse en forma de lista.

Adicionalmente, otra de las funciones de este TODO es que al darle clic al emoji (actividad), éste se debe remover de la lista. Puedes tomar este diseño como referencia:

![dribbble-todo-list](https://cdn.dribbble.com/users/1977127/screenshots/4522954/daily_ui_challenge_042_800x600.png)

## Listado de música

Esta página debe mostrar las canciones de uno de los álbumes de tu artista favorito.

Adicional, deben existir 2 botones para cambiar el diseño de las canciones mostradas, ya sea para verlas en cuadrícula o en lista. Puedes tomar este diseño como referencia:

![dribbble-podcast](https://cdn.dribbble.com/users/1977127/screenshots/4532391/daily_ui_challenge_044_800x600.png)

------

¡Espero tus soluciones en la sección de comentarios!

# 3. Cómo presentar entrevistas técnicas

## Recomendaciones generales para una entrevista técnica

- Primer paso para realizar la entrevista - Es conseguirla.
- Tener las habilidades adecuadas
- Codear, revisar el codigo, trucos estes haciendo constantemente.
- Preguntas de design, todo el tiempo.
- Conocimiento === Tiempo libre, pasion, experiencia.
- Venderte a ti mismo [ Lo bueno de ti, experiencia ]
- Seguridad de ti, mismo.
- Profundizar las respuesta, buena actitud.
  - Escucha sugerenicas
  - Se puntual
  - Prende la camara
  - Haz preguntas sobre el empleo, sobre la empresa.
- Se honesto.
- Confia en ti.

## Buenas prácticas de HTML, CSS y JavaScript para una entrevista

1. Js
2. CSS
3. HTML

Preguntas especificas del Lenguaje.

> Frase de Quien quiere ser millonario
>
> >  "Si no saben por favor no voten, Si saben voten ya".

Muestrate tranquilo y seguro.

__Las preguntas de entrevista estan disenadas para ver que tan profundo est tu conocimietno en General.__

[![img](https://www.google.com/s2/favicons?domain=https://miro.medium.com/fit/c/152/152/1*sHhtYhaCe2Uc3IU0IgKwIQ.png)The Ultimate Study Guide For Front End Interview | by Chen Reuven | Medium](https://medium.com/@chen.reuven/the-ultimate-study-guide-for-front-end-interview-776fa3ead1b3)

## HTML, CSS y JavaScript a nivel práctico para entrevistas

- Ubicate en un lugar cómodo
- Ten listo tu ambiente. Trata de usar herramientas con las que ya estés familiarizada
- Concéntrate y que comience el juego

En HTML/CSS lo mas común es que te den un tiempo para maquetar una landing page. Prepara tu HTML con buenas prácticas desde el principio, verifica bien tu código desde el principio.

En Javascript, la mayoría son ejercicios de algoritmos, puedes practicar esto en paginas como leetcode, hackerank, codewars etc

[![img](https://www.google.com/s2/favicons?domain=https://www.lapa.ninja/apple-touch-icon-57x57.png)Flamingo landing page design inspiration - Lapa Ninja](https://www.lapa.ninja/post/shopflamingo/)

[![img](https://www.google.com/s2/favicons?domain=https://static.codingame.com/assets/favicon_16_16.6776a532.png)Coding Games and Programming Challenges to Code Better](https://www.codingame.com/start)

[![img](https://www.google.com/s2/favicons?domain=https://leetcode.com//apple-touch-icon-57x57.png)LeetCode - The World's Leading Online Programming Learning Platform](https://leetcode.com/)

## Mejores prácticas para presentar entrevistas técnicas

Los procesos de entrevistas varían dependiendo de cada una de las empresas a las que apliquemos, sin embargo, tener conocimientos sólidos de las diferentes tecnologías y conocimiento general de cómo serán estas entrevistas, pueden hacer que logremos procesos exitosos.

A nivel general, un proceso de entrevista está conformado por:

- Entrevista técnica
- Prueba técnica
- Entrevista cultural

También, pueden estar involucradas algunas llamadas telefónicas donde se conozca al candidato, se haga una negociación de salario, se hable de beneficios, se consolide la fecha de ingreso, entre otras.

Te hablaré un poco sobre ellas para que logres los mejores resultados:

## Entrevista técnica

En este tipo de entrevistas, el entrevistador es quién hace preguntas o ejercicios en vivo para conocer tus habilidades técnicas en: HTML, CSS, JS, performance, accesibilidad, entre otras relacionadas con el desarrollo frontend.

Para este tipo de pruebas, pregunta lo que no conozcas, trata de pensar en voz alta, que no te dé miedo decir “no sé” (aquí puedes decir algo como: “no sé, pero por lo que he oído sobre X puedo suponer que puede ser Y”, es decir, no solo digas “no sé” sino que trata de aproximarte a alguna respuesta), y muéstrate siempre como una persona a la que le gusta aprender.

Aquí te comparto algunas preguntas que te pueden ser útiles para este tipo de entrevistas. Eso sí, no te recomiendo aprenderlas de memoria porque normalmente el entrevistador es una persona que conoce sobre lo que está preguntando, así que, fácilmente puede profundizar muchísimo más y ahí es donde deberás demostrar que has tenido experiencia y una simple respuesta aprendida no te ayudará mucho.

- [Front End Interview Handbook](https://frontendinterviewhandbook.com/introduction/)
- [Frontend developer interview questions](https://github.com/h5bp/Front-end-Developer-Interview-Questions)
- [Frontend masters interview questions](https://frontendmasters.com/books/front-end-handbook/2018/practice/interview-q.html)
- [Course Burst: clearing your frontend job interview](https://codeburst.io/clearing-your-front-end-job-interview-javascript-d5ec896adda4)

## Prueba técnica

Las pruebas técnicas son normalmente pequeñas aplicaciones para hacer desde casa en cierto tiempo y sin la presión de estar siendo observado(a). En este caso, confía en tus conocimientos y demuestra él(la) excelente desarrollador(a) frontend que llevas dentro. Elabora el mejor código limpio y con las mejores prácticas posibles, como también, realiza el 100% de los requerimientos solicitados. Si no entiendes algo del problema, ten la confianza y seguridad de preguntarles (¡ellos van a ser tus futuros compañeros de trabajo!) para lograr el mejor resultado tanto visual como funcional.

En este tipo de pruebas, también notarás si eres apto(a) o no para el cargo al que estás aplicando.

Si ves que te demorás más del tiempo solicitado o si ves la necesidad de pedirle ayuda a alguien, es mejor que des un paso atrás y considera este proceso como aprendizaje para detectar en qué cosas aún debes profundizar, y como una experiencia más que te da la oportunidad de mejora para el futuro.

## Entrevista cultural

Esta entrevista parece ser la más sencilla de todas, pero no la subestimemos. En esta entrevista (aunque te haya ido muy bien en las entrevistas anteriores) puede hacer que el entrevistador desista de una posible contratación.

Lo ideal para esta entrevista es demostrar que somos él(la) candidato(a) perfecto(a) para ese puesto, aquí debemos impresionar con nuestro perfil y nuestras capacidades. Para ello, prepara:

- Un breve párrafo sobre quién eres y sobre tu trayectoria.
- Una lista de cosas importantes que has logrado en tu carrera como desarrollador(a) frontend (features que hayas realizado y de los que te sientes muy orgulloso(a)).
- Una lista de errores que hayas cometido y la correspondiente solución que le diste a cada uno.
- Preguntas sobre la empresa y sobre el proceso para demostrar tu interés. Estas pueden estar relacionadas con la forma de trabajo, la conformación de los equipos, tecnologías con las que trabajan, etc.

No olvides ser honesto(a) con tus habilidades y mostrarte tal cual eres.

Estas fueron algunas de mis recomendaciones. Espero te sean muy útiles y recuerda siempre las siguientes frases:

**Todos los grandes líderes de la industria, también fueron rechazados en algún momento.**

**Todos los grandes líderes de la industria, también tuvieron que empezar por algún lado.**

**Todos los grandes líderes de la industria, también tuvieron que estar en varios procesos para encontrar la empresa de sus sueños.**

# 4. Cómo resolver pruebas técnicas en casa

## Entendimiento general del problema

Buscando tu hijo en GitHub

**Problema:**

Imagina que res Elon Musk y deseas saber si alguna persona en el mundo con cuenta en GitHub ha creado algun repositorio que tenga como nombre alguna letra del nombre de su ultimo hijo XAE A-Xii.

**Historia de Usuario**

Como Elon Musk, quiero ingresar en un formulario el usuario y el rol de la persona a la que quiero revisar el GitHub para que guarde esta informacion en LocalStorage y posteriormente sea visible al comienzo de la pagina.

**Historia**

Como Elon Musk, deseo buscar en todos los repositorios publicos de ese usuario si alguno coincide con alguna letra del nombre de mi ultimo hijo XAE A-Xii.

**Historia**

Como Elon Musk, deso ver todos los repositorios publicos de ese usuario en una tabla si no obtuve coincidencias. Esta tabla debe contener: el nombre del repositorio, la description y el lenguaje.

**Requerimientos**

- Ser compatible con Chrome, Safari y Firefox.

- Usar cualquier framework o libreria de JavaScript.

- Implementar algun preprocesador.

- No usar fremeworks de CSS



## Elección del framework para realizar la prueba

Haz tu prueba con el lenguage, framework o libreria que mejor te sinetas. No inventes a ultimo momento.

- **React, Angular y Vue**

[![img](https://www.google.com/s2/favicons?domain=//static.platzi.com/media/favicons/platzi_favicon.png)React vs. Angular vs. Vue vs. Svelte, ¿cuál es mejor?](https://platzi.com/blog/react-angular-vue-svelte)

## Cómo ejecutar una prueba técnica

**8 Aspectos tecnicos para resolver la prueba tecnica**

1. Haz que funcione y luego lo pones bonito.
2. Si tiene diseno cuida cada detalle.
3. Entregar en el tiempo acordado.
4. Cumple con el 100% de los requerimientos.
5. Realizar el deploy. Puedes usar github pages para esto, también está Netlify, Heroku etc
6. Haz tu código 100% en inglés. Si necesitas agregar comentarios en tu codigo deberia estar en inglés
7. Usa las mejores practicas
8. Haz un buen README.

## Preguntas y respuestas sobre pruebas técnicas en casa

**Cuando fue tu promer empleo?**

​	Buscar constantemente y aplicar.

**Se puede trabajar solo en JavaScript?**

​	Si. Es mejor aprender algun Framework.

**Aprender JavaScript full?**

 	Luego aprender Framework. Intercambiar el aprendizaje.

**Entrevistas tecnicas, es valido consultar por google?**

​	Comunicar, si es posible revisar y consultar a google.

**Introduccion al proyecto**

​	Estimacion de tiempo de lectura. De actividas, luego te se asignan las funciones realizar.

**Estimaciones en demora el proyecto?**

​	Visualizar caundo tardamos en realizar un boton y asignarles estilos.

**A que oportunidades laborales no aplicar?** 

​	Preguntar a la comunidad si han trabajo, para poder optar al trabajo.

# 5. Experiencias, historias y traumas personales

## Cómo estar al día en tecnología: comunidades, conferencias, blogs y más

- Blogs post
- Conferencias
- Bootcamps
- Charlas
- meetup
- Companias

## Proyectos individuales y contribución a proyectos open source

- Socializar
- Aprender nuevas habilidades
- Daydreaming
- Rompe tu rutina
- Consigue un Hobby
- Ponte un Reto
- Ama lo que haces
- Acpeta Retroalimentaci&oacute;n
- Cuestionan las cosas
- Termina tus Proyectos
- Deja de compararte con otros
- Apaga todos los distractores

## Mi experiencia en entrevistas: lo que hubiera hecho (y lo que no)

[![img](https://www.google.com/s2/favicons?domain=https://html.com/wp-content/uploads/cropped-site-icon-32x32.png)How to Ace a Front-End Developer Interview](https://html.com/resources/front-end-dev-interview/)

## Preguntas y respuestas sobre cómo estar al día

**Como participar en comunidades?**

1. meetup
1. Talleres o conferencias online.

**Saber Ingles para ingresar a comunidades?**

​	Traductores personales, pero es recomendable saber ce Ingles.

**Que compartir con la comunidad?**

​	Compartir contenido de cualquier cosa, de lo que se esta estudiando.

**Linkedin es una herramienta para obtener empleo?**

​	Si, es una oportunidad y estas cerca del empleo.

**Proyectos mas complicados?**

​	Proceso mas extenso.