<h1>Frontend Developer</h1>

<h3>Estefany Aguilar</h3>

<h1>Tabla de Contenido</h1>

- [1. Layout y componentes](#1-layout-y-componentes)
  - [Identifica las pantallas de tu proyecto](#identifica-las-pantallas-de-tu-proyecto)
  - [Sistema de diseño, assets y variables de CSS](#sistema-de-diseño-assets-y-variables-de-css)
- [2. Maquetación responsiva: pantallas de autenticación](#2-maquetación-responsiva-pantallas-de-autenticación)
  - [Crear nueva contraseña: HTML](#crear-nueva-contraseña-html)
  - [Crear nueva contraseña: CSS](#crear-nueva-contraseña-css)
  - [Email enviado](#email-enviado)
  - [Login](#login)
  - [Crear y editar mi cuenta](#crear-y-editar-mi-cuenta)
  - [Mi cuenta](#mi-cuenta)
- [3. Maquetación responsiva: vistas principales](#3-maquetación-responsiva-vistas-principales)
  - [Página de inicio: HTML](#página-de-inicio-html)
  - [Página de inicio: CSS](#página-de-inicio-css)
  - [Menú desktop](#menú-desktop)
  - [Menú mobile](#menú-mobile)
  - [Mi orden: HTML](#mi-orden-html)
  - [Mi orden: CSS](#mi-orden-css)
  - [Mis órdenes](#mis-órdenes)
  - [Navbar: HTML](#navbar-html)
  - [Navbar: CSS](#navbar-css)
  - [Detalle de producto](#detalle-de-producto)
  - [Carrito de compras: HTML](#carrito-de-compras-html)
- [4. Próximos pasos](#4-próximos-pasos)
  - [Cómo continuar aprendiendo desarrollo](#cómo-continuar-aprendiendo-desarrollo)


# 1. Layout y componentes

## Identifica las pantallas de tu proyecto

[![img](https://www.google.com/s2/favicons?domain=https://scene.zeplin.io/project/60afeeed20af1378ed046538/img/favicon/apple-touch-icon-57x57.png)Yard Sale | Zeplin Scene](https://scene.zeplin.io/project/60afeeed20af1378ed046538)

[![img](https://www.google.com/s2/favicons?domain=https://static.figma.com/app/icon/1/icon-192.png)Figma](https://www.figma.com/proto/bcEVujIzJj5PNIWwF9pP2w/Platzi_YardSale?node-id=0%3A684&scaling=scale-down&page-id=0%3A1&starting-point-node-id=0%3A719)

[![img](https://www.google.com/s2/favicons?domain=https://static.figma.com/app/icon/1/icon-192.png)Figma](https://www.figma.com/proto/bcEVujIzJj5PNIWwF9pP2w/Platzi_YardSale?node-id=3%3A2112&scaling=scale-down&page-id=0%3A998&starting-point-node-id=5%3A2808)

## Sistema de diseño, assets y variables de CSS

[Enlace para descargar las carpetas con los iconos y logos:](https://drive.google.com/drive/folders/1EqPBh8LR0TJIi3zJCwPxl8W9h8Mc82GS?usp=sharing)

Código para nuestras variables

```css
:root{
    --white: #FFFFFF;
    --black: #000000;
    --dark: #232830;
    --very-light-pink: #C7C7C7;
    --text-input-field: #F7F7F7;
    --hospital-green: #ACD9B2;
}

body{
    font-family: 'Quicksand', sans-serif;
}
```

[![img](https://www.google.com/s2/favicons?domain=https://polaris.shopify.com/design/design/favicon.ico)Design - Shopify Polaris](https://polaris.shopify.com/design/design)

[![img](https://www.google.com/s2/favicons?domain=https://www.gstatic.com/images/icons/material/apps/fonts/1x/catalog/v5/favicon.svg)Google Fonts](https://fonts.google.com/specimen/Quicksand)

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)GitHub - platzi/curso-frontend-developer-practico](https://github.com/platzi/curso-frontend-developer-practico)

# 2. Maquetación responsiva: pantallas de autenticación

## Crear nueva contraseña: HTML

En este curso decidimos NO crear archivos con extensión `.css` para guardar nuestros estilos, sino guardarlos en los mismos archivos `.html` (dentro de la etiqueta `<style>`).

Si lo prefieres, puedes hacer esta división con tus propios archivos `.css`. De hecho, es la forma más recomendada de trabajar profesionalmente con HTML y CSS.

CSS DE LA CLASE.

```css
:root {
      --white: #FFFFFF;
      --black: #000000;
      --very-light-pink: #C7C7C7;
      --text-input-field: #F7F7F7;
      --hospital-green: #ACD9B2;
      --sm: 14px;
      --md: 16px;
      --lg: 18px;
    }
    body {
      margin: 0;
      font-family: 'Quicksand', sans-serif;
    }
    .login {
      width: 100%;
      height: 100vh;
      display: grid;
      place-items: center;
    }
    .form-container {
      display: grid;
      grid-template-rows: auto 1fr auto;
      width: 300px;
    }
    .logo {
      width: 150px;
      margin-bottom: 48px;
      justify-self: center;
      display: none;
    }
    .title {
      font-size: var(--lg);
      margin-bottom: 12px;
      text-align: center;
    }
    .subtitle {
      color: var(--very-light-pink);
      font-size: var(--md);
      font-weight: 300;
      margin-top: 0;
      margin-bottom: 32px;
      text-align: center;
    }
    .form {
      display: flex;
      flex-direction: column;
    }
    .label {
      font-size: var(--sm);
      font-weight: bold;
      margin-bottom: 4px;
    }
    .input {
      background-color: var(--text-input-field);
      border: none;
      border-radius: 8px;
      height: 30px;
      font-size: var(--md);
      padding: 6px;
      margin-bottom: 12px;
    }
    .primary-button {
      background-color: var(--hospital-green);
      border-radius: 8px;
      border: none;
      color: var(--white);
      width: 100%;
      cursor: pointer;
      font-size: var(--md);
      font-weight: bold;
      height: 50px;
    }
    .login-button {
      margin-top: 14px;
      margin-bottom: 30px;
    }
    @media (max-width: 640px) {
      .logo {
        display: block;
      }
    }
```

[![img](https://www.google.com/s2/favicons?domain=https://github.com/fluidicon.png)curso-frontend-developer-practico/clase1.html at main · platzi/curso-frontend-developer-practico · GitHub](https://github.com/platzi/curso-frontend-developer-practico/blob/main/clase1.html)

## Crear nueva contraseña: CSS

CSS DE LA CLASE:

```css
:root {
      --white: #FFFFFF;
      --black: #000000;
      --very-light-pink: #C7C7C7;
      --text-input-field: #F7F7F7;
      --hospital-green: #ACD9B2;
      --sm: 14px;
      --md: 16px;
      --lg: 18px;
    }
    body {
      margin: 0;
      font-family: 'Quicksand', sans-serif;
    }
    .login {
      width: 100%;
      height: 100vh;
      display: grid;
      place-items: center;
    }
    .form-container {
      display: grid;
      grid-template-rows: auto 1fr auto;
      width: 300px;
      justify-items: center;
    }
    .logo {
      width: 150px;
      margin-bottom: 48px;
      justify-self: center;
      display: none;
    }
    .title {
      font-size: var(--lg);
      margin-bottom: 12px;
      text-align: center;
    }
    .subtitle {
      color: var(--very-light-pink);
      font-size: var(--md);
      font-weight: 300;
      margin-top: 0;
      margin-bottom: 32px;
      text-align: center;
    }
    .email-image {
      width: 132px;
      height: 132px;
      border-radius: 50%;
      background-color: var(--text-input-field);
      display: flex;
      justify-content: center;
      align-items: center;
      margin-bottom: 24px;
    }
    .email-image img {
      width: 80px;
    }
    .resend {
      font-size: var(--sm);
    }
    .resend span {
      color: var(--very-light-pink);
    }
    .resend a {
      color: var(--hospital-green);
      text-decoration: none;
    }
    .primary-button {
      background-color: var(--hospital-green);
      border-radius: 8px;
      border: none;
      color: var(--white);
      width: 100%;
      cursor: pointer;
      font-size: var(--md);
      font-weight: bold;
      height: 50px;
    }
    .login-button {
      margin-top: 14px;
      margin-bottom: 30px;
    }
    @media (max-width: 640px) {
      .logo {
        display: block;
      }
    }
```

Recomendación que dejó Diego De Granda en el Curso de Responsive Design: Maquetación Mobile First, es escribir los estilos ordenados según su propósito de la siguiente forma:

1. Posicionamiento
2. Box Model
3. Tipografía
4. Visuales
5. Otros
   Y cada categoría se separa con un espacio, de forma que resulta mas legible. Por ejemplo:

```css
display: grid;
  place-items: center;

  width: 100%;
  height: 100vh;

font-size: 1rem;
```

## Email enviado

`email.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>submit</title>
  <!-- fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

  <!-- styles -->
  <link rel="stylesheet" href="./style/main.css">
</head>
<body>
  <div class="login">
    <div class="form-container">
      <img src="./logos/logo_yard_sale.svg" alt="logo" class="logo">

      <h1 class="title">Email has been sent!</h1>
      <p class="subtitle">Please check your inbox for instructions on how to reset the password</p>

      <div class="email-image">
        <img src="./icons/email.svg" alt="email">
      </div>

      <button class="primary-button login-button">Login</button>

      <p class="resend">
        <span>Didn't recive the email? <a href="/">Resend</a></span>
      </p>
    </div>
  </div>
</body>
</html>
```

`main.css`

```css

.email-image {
  width: 132px;
  height: 132px;
  border-radius: 50%;
  background-color: var(--text-input-field);
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 24px;
}

.email-image img {
  width: 80px;
}

.resend {
  font-size: var(--sm);
}

.resend span {
  color: var(--very-light-pink);
}

.resend a {
  color: var(--hospital-green);
  text-decoration: none;
}
```

## Login

```css
@media (max-width: 640px){
    
    .logo {
        display: block;
    }

    .secondary-button {
        width: 300px;
        position: absolute;
        bottom: 0;
        margin-bottom: 24px;
    }
}
```

Aquí el resultado final:
![Captura de Pantalla 2021-09-30 a la(s) 19.09.10.png](https://static.platzi.com/media/user_upload/Captura%20de%20Pantalla%202021-09-30%20a%20la%28s%29%2019.09.10-24a78b68-436a-41ec-9f0e-30df725c7dc3.jpg)

## Crear y editar mi cuenta

Estilos de esta clase:

```
:root {
  --white: #ffffff;
  --black: #000000;
  --very-light-pink: #c7c7c7;
  --text-input-field: #f7f7f7;
  --hospital-green: #acd9b2;
  --sm: 14px;
  --md: 16px;
  --lg: 18px;
}
body {
  margin: 0;
  font-family: "Quicksand", sans-serif;
}
.login {
  width: 100%;
  height: 100vh;
  display: grid;
  place-items: center;
}
.form-container {
  display: grid;
  grid-template-rows: auto 1fr auto;
  width: 300px;
}
.logo {
  width: 150px;
  margin-bottom: 48px;
  justify-self: center;
  display: none;
}
.title {
  font-size: var(--lg);
  margin-bottom: 36px;
  text-align: start;
}
.form {
  display: flex;
  flex-direction: column;
}
.form div {
  display: flex;
  flex-direction: column;
}
.label {
  font-size: var(--sm);
  font-weight: bold;
  margin-bottom: 4px;
}
.input {
  background-color: var(--text-input-field);
  border: none;
  border-radius: 8px;
  height: 30px;
  font-size: var(--md);
  padding: 6px;
  margin-bottom: 12px;
}
.input-name,
.input-email,
.input-password {
  margin-bottom: 22px;
}
.primary-button {
  background-color: var(--hospital-green);
  border-radius: 8px;
  border: none;
  color: var(--white);
  width: 100%;
  cursor: pointer;
  font-size: var(--md);
  font-weight: bold;
  height: 50px;
}
.login-button {
  margin-top: 14px;
  margin-bottom: 30px;
}
@media (max-width: 640px) {
  .form-container {
    height: 100%;
  }
  .form {
    height: 100%;
    justify-content: space-between;
  }
}
```

```css
@media (max-width: 640px){
      .logo{
        display: block;
      }
      .secundary-button{
        position: fixed;
        bottom: 0;
        width: inherit;
        margin-bottom: 30px;
      }
    }
```



## Mi cuenta

![screenMyAccount.png](https://static.platzi.com/media/user_upload/screenMyAccount-7750a719-dc6e-42bc-8023-56710aa0b4f0.jpg)

HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet">
    
    <title>Document</title>
    <style>
        :root {
            --black: #000000;
            --white: #ffffff;
            --very-light-pink: #C7C7C7;
            --text-input-field: #F7F7F7;
            --hospital-green: #acd0b2;
            --primary-button-hover: #5ab865;
            --sm: 14px;
            --md: 16px;
            --lg: 18px;
        }

        body {
            font-family: 'Quicksand', sans-serif;
            margin:0;
            padding:0
        }

        .login{
            width:100%;
            height: 100vh;
            display: grid;
            place-items: center ;
        }

        .container{  
            display: grid;
            grid-template-rows: auto 1fr auto;
            width: 300px;
        }

       

        .title{
            font-size: var(--lg);
            margin-bottom: 40px;
            text-align: start;
        }


        .form{
            display: flex;
            flex-direction: column ;
        }

        .label {
            font-size: var(--sm);
            font-weight: bold;
            margin-bottom: 4px;
        }

        .value{
            color: var(--very-light-pink);
            font-size: var(--md);
            margin: 8px 0 32px 0;
        }
        
        .primary-button{
            margin-top: 24px;
            
            height: 40px;
            width: 300px;
            border: 1px solid var(--hospital-green);
            border-radius: 12px;
            

            color: var(--hospital-green);
            font-size: var(--md);
            font-weight: bold;

            background-color: var(--white);

            cursor: pointer;


        }

        .primary-button:hover{
            background-color: var(--primary-button-hover);
            color: var(--white);
        }

        @media (max-width: 640px){
            .logo{
                display: block;
            }

            .primary-button{
                position: absolute;
                bottom: 0;
                margin-bottom: 24px;
            }
        }
        
    </style>
</head>
<body>
    <div class="login">
        <div class="container">
            <h1 class="title">
                My account
            </h1>
            
            <form action="" class="form">
                <label for="name" class="label">
                    Name
                </label>
                <p class="value">
                    Camila Yokoo
                </p>

                <label for="email" class="label">
                    Email address
                </label>
                <p class="value" >
                    camilayokoo@gmail.com
                </p>

                <label for="password" class="label">
                    Password
                </label>
                <p class="value">
                    ***********
                </p>
                
                <input type='submit' value="Edit" class="primary-button login-button" />
            </form>
        </div>
    </div>
</body>
</html>
```

# 3. Maquetación responsiva: vistas principales

## Página de inicio: HTML

[pexel](https://www.pexels.com/es-es/)

## Página de inicio: CSS

Acá el código de la clase

**HTML**

```html
<main>
        <section class="main-container">
            <div class="cards-container">
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
                <div class="product-card">
                    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="">
                    <div class="product-info">
                        <div>
                            <p>$120,00</p>
                            <p>Bike</p>
                        </div>
                        <figure>
                            <img src="./Platzi_YardSale_Icons/bt_add_to_cart.svg" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </section>
    </main>
```

**CSS**

```css
.cards-container {
    display: grid;
    grid-template-columns: repeat(auto-fill,240px);
    gap: 25px;
    place-content: center;
}

.product-card {
    width: 240px;
}

.product-card img {
    width: 240px;
    height: 240px;
    border-radius: 20px;
    object-fit: cover;
}

.product-info {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 12px;
}

.product-info div p:nth-child(1) {
    font-weight: bold;
    font-size: 14px;
    margin: 0 0 4px 0;
}

.product-info div p:nth-child(2) {
    font-size: 12px;
    color: var(--very-light-pink);
}

.product-info figure {
    margin: 0;
}

.product-info figure img {
    width: 40px;
    height: 40px;
} 

@media (max-width: 640px) {
    .cards-container {
        display: grid;
        grid-template-columns: repeat(auto-fill,140px);
    }

    .product-card {
        width: 140px;
    
    }

    .product-card img {
        width: 100%;
        height: 140px;
    }
}
```

## Menú desktop

Maquetado de la clase
**HTML**

```css
.desktop-menu {
    width: 100px;
    height: auto;
    border: 1px solid var(--very-light-pink);
    border-radius: 6px;
    padding: 20px 20px 0 20px;
}

.desktop-menu ul {
    list-style: none;
    padding: 0;
    margin: 0;
}

.desktop-menu ul li {
    text-align: end;
}

.desktop-menu ul li:nth-child(1), 
.desktop-menu ul li:nth-child(2) {
    font-weight: bold;
}

.desktop-menu ul li:last-child {
    padding-top: 20px;
    border-top: 1px solid var(--very-light-pink);
}

.desktop-menu ul li:last-child a {
    color: var(--hospital-green);
    font-size: 12px;
}

.desktop-menu ul li a {
    color: var(--black);
    margin-bottom: 20px;
    display: inline-block;
    text-decoration: none;
}
```

## Menú mobile

`html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Menu mobile</title>

   <!-- fonts -->
   <link rel="preconnect" href="https://fonts.googleapis.com" />
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
   <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

   <style>
    :root {
      --very-light-pink: #c7c7c7;
      --text-input-field: #f7f7f7;
      --hospital-green: #acd992;
      --white: #ffffff;
      --black: #000000;
      --dark: #232330;
    
      /* fonts */
      --sm: 14px;
      --md: 16px;
      --lg: 18px;
    }
    
    body {
      margin: 0;
      font-family: 'Quicksand', sans-serif;
    }
    .mobile-menu {
      padding: 24px;
    }
    .mobile-menu a {
      text-decoration: none;
      color: var(--black);
      font-weight: bold;
    }
    .mobile-menu ul {
      padding: 0;
      margin: 24px 0 0;
      list-style: none;
    }

    .mobile-menu ul:nth-child(1) {
      border-bottom: 1px solid var(--very-light-pink);
    }

    .mobile-menu ul li {
      margin-bottom: 24px;
    }
    .mobile-menu .email {
      font-size: var(--sm);
      font-weight: 300 !important;
    }
    .sign-out {
      color: var(--hospital-green) !important;
      font-size: var(--sm);
    }
    .mobile-menu ul:nth-child(3) {
      position: absolute;
      bottom: 0;
      }

   </style>
</head>
<body>
  <div class="mobile-menu">
    <ul>
      <li><a href="/">CATEGORIES</a></li>
      <li><a href="/">ALL</a></li>
      <li><a href="/">Clothes</a></li>
      <li><a href="/">Electronics</a></li>
      <li><a href="/">Furnitures</a></li>
      <li><a href="/">Toys</a></li>
      <li><a href="/">Othes</a></li>
    </ul>

    <ul>
      <li><a href="/">My order</a></li>
      <li><a href="/">My account</a></li>
    </ul>
    <ul>
      <li><a href="/" class="email">platzi@example.com</a></li>
      <li><a href="/" class="sign-out">Sign out</a></li>
    </ul>
  </div>
</body>
</html>
```

## Mi orden: HTML

`html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Order</title>

   <!-- fonts -->
   <link rel="preconnect" href="https://fonts.googleapis.com" />
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
   <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

   <style>
    :root {
      --very-light-pink: #c7c7c7;
      --text-input-field: #f7f7f7;
      --hospital-green: #acd992;
      --white: #ffffff;
      --black: #000000;
      --dark: #232330;
    
      /* fonts */
      --sm: 14px;
      --md: 16px;
      --lg: 18px;
    }
    
    body {
      margin: 0;
      font-family: 'Quicksand', sans-serif;
    }
   </style>
</head>
<body>
  <div class="my-order">
    <div class="my-order-container">
      <div class="my-order-content">
       <div>
         <p>
           <span>03.25.21</span>
           <span>6 article</span>
          </p>
          <p>$560.00</p>
        </div>
      </div>

      <div class="shopping-card">
        <figure>
          <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="bike">
        </figure>
        <p>Bike</p>
        <p>$30.00</p>
      </div>
    </div>
  </div>
</body>
</html>
```



## Mi orden: CSS

`html`

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Order</title>

    <!-- fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

    <style>
      :root {
        --very-light-pink: #c7c7c7;
        --text-input-field: #f7f7f7;
        --hospital-green: #acd992;
        --white: #ffffff;
        --black: #000000;
        --dark: #232330;

        /* fonts */
        --sm: 14px;
        --md: 16px;
        --lg: 18px;
      }

      body {
        margin: 0;
        font-family: 'Quicksand', sans-serif;
      }

      .my-order {
        width: 100%;
        height: 100vh;
        display: grid;
        place-items: center;
      }

      .title {
        font-size: var(--lg);
        margin-bottom: 40px;
      }

      .my-order-container {
        display: grid;
        grid-template-rows: auto 1fr;
        width: 300px;
      }
      .my-order-content {
        display: flex;
        flex-direction: column;
      }

      .order {
        display: grid;
        grid-template-columns: auto 1fr;
        gap: 16px;
        align-items: center;
        background-color: var(--text-input-field);
        margin-bottom: 24px;
        border-radius: 8px;
        padding: 0 24px;
      }

      .order p:nth-child(1) {
        display: flex;
        flex-direction: column;
      }

      .order p span:nth-child(1) {
        font-size: var(--md);
        font-weight: bold;
      }

      .order p span:nth-child(2) {
        font-size: var(--sm);
        color: var(--very-light-pink);
      }

      .order p:nth-child(2) {
        text-align: end;
        font-weight: bold;
      }

      .shopping-card {
        display: grid;
        grid-template-columns: auto 1fr auto auto;
        gap: 16px;
        margin-bottom: 24px;
        align-items: center;
      }

      .shopping-card figure {
        margin: 0;
      }

      .shopping-card figure img {
        width: 70px;
        height: 70px;
        border-radius: 20px;
        object-fit: cover;
      }
      .shopping-card p:nth-child(2) {
        color: var(--very-light-pink);
      } 
      .shopping-card p:nth-child(3) {
        font-size: var(--md);
        font-weight: bold;
      } 
    </style>
  </head>
  <body>
    <div class="my-order">
      <div class="my-order-container">
        <h1 class="title">My order</h1>

        <div class="my-order-content">
          <div class="order">
            <p>
              <span>03.25.21</span>
              <span>6 article</span>
            </p>
            <p>$560.00</p>
          </div>

          <div class="shopping-card">
            <figure>
              <img
                src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                alt="bike"
              />
            </figure>
            <p>Bike</p>
            <p>$30.00</p>
          </div>

          <div class="shopping-card">
            <figure>
              <img
                src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                alt="bike"
              />
            </figure>
            <p>Bike</p>
            <p>$30.00</p>
          </div>

          <div class="shopping-card">
            <figure>
              <img
                src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                alt="bike"
              />
            </figure>
            <p>Bike</p>
            <p>$30.00</p>
          </div>
          
          <div class="shopping-card">
            <figure>
              <img
                src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                alt="bike"
              />
            </figure>
            <p>Bike</p>
            <p>$30.00</p>
          </div>

          <div class="shopping-card">
            <figure>
              <img
                src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                alt="bike"
              />
            </figure>
            <p>Bike</p>
            <p>$30.00</p>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
```



## Mis órdenes

`html`

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Order</title>

    <!-- fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

    <style>
      :root {
        --very-light-pink: #c7c7c7;
        --text-input-field: #f7f7f7;
        --hospital-green: #acd992;
        --white: #ffffff;
        --black: #000000;
        --dark: #232330;

        /* fonts */
        --sm: 14px;
        --md: 16px;
        --lg: 18px;
      }

      body {
        margin: 0;
        font-family: 'Quicksand', sans-serif;
      }

      .my-order {
        width: 100%;
        height: 100vh;
        display: grid;
        place-items: center;
      }

      .title {
        font-size: var(--lg);
        margin-bottom: 40px;
      }

      .my-order-container {
        display: grid;
        grid-template-rows: auto 1fr;
        width: 300px;
      }
      .my-order-content {
        display: flex;
        flex-direction: column;
      }

      .order {
        display: grid;
        grid-template-columns: auto 1fr auto;
        gap: 16px;
        align-items: center;
        margin-bottom: 12px;
      }

      .order p:nth-child(1) {
        display: flex;
        flex-direction: column;
      }

      .order p span:nth-child(1) {
        font-size: var(--md);
        font-weight: bold;
      }

      .order p span:nth-child(2) {
        font-size: var(--sm);
        color: var(--very-light-pink);
      }

      .order p:nth-child(2) {
        text-align: end;
        font-weight: bold;
      }

    </style>
  </head>
  <body>
    <div class="my-order">
      <div class="my-order-container">
        <h1 class="title">My orders</h1>

        <div class="my-order-content">
          <div class="order">
            <p>
              <span>03.25.21</span>
              <span>6 article</span>
            </p>
            <p>$560.00</p>
            <img src="./icons/flechita.svg" alt="arrow">
          </div>


          <div class="order">
            <p>
              <span>03.25.21</span>
              <span>6 article</span>
            </p>
            <p>$560.00</p>
            <img src="./icons/flechita.svg" alt="arrow">
          </div>

          <div class="order">
            <p>
              <span>03.25.21</span>
              <span>6 article</span>
            </p>
            <p>$560.00</p>
            <img src="./icons/flechita.svg" alt="arrow">
          </div>

          <div class="order">
            <p>
              <span>03.25.21</span>
              <span>6 article</span>
            </p>
            <p>$560.00</p>
            <img src="./icons/flechita.svg" alt="arrow">
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
```

## Navbar: HTML

`html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>

   <!-- fonts -->
   <link rel="preconnect" href="https://fonts.googleapis.com" />
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
   <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

   <style>
    :root {
      --very-light-pink: #c7c7c7;
      --text-input-field: #f7f7f7;
      --hospital-green: #acd992;
      --white: #ffffff;
      --black: #000000;
      --dark: #232330;
    
      /* fonts */
      --sm: 14px;
      --md: 16px;
      --lg: 18px;
    }
    
    body {
      margin: 0;
      font-family: 'Quicksand', sans-serif;
    }
   </style>
</head>
<body>
  <nav>
    <img src="./icons/icon_menu.svg" alt="menu" class="menu">

    <div class="navabar-left">
      <img src="./logos/logo_yard_sale.svg" alt="logo" class="logo">

      <ul>
        <li><a href="/">All</a></li>
        <li><a href="/">Clothes</a></li>
        <li><a href="/">Electronics</a></li>
        <li><a href="/">Furnitures</a></li>
        <li><a href="/">Toys</a></li>
        <li><a href="/">Others</a></li>
      </ul>
    </div>

    <div class="navbar-right">
      <li>platzi@example.com</li>
      <li class="navbar-shopping-cart">
        <img src="./icons/icon_shopping_cart.svg" alt="shopping cart">
        <div>2</div>
      </li>
    </div>
  </nav>
</body>
</html>
```

## Navbar: CSS

`html`

```HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>NavBar</title>

   <!-- fonts -->
   <link rel="preconnect" href="https://fonts.googleapis.com" />
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
   <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

   <style>
    :root {
      --very-light-pink: #c7c7c7;
      --text-input-field: #f7f7f7;
      --hospital-green: #acd992;
      --white: #ffffff;
      --black: #000000;
      --dark: #232330;
    
      /* fonts */
      --sm: 14px;
      --md: 16px;
      --lg: 18px;
    }
    
    body {
      margin: 0;
      font-family: 'Quicksand', sans-serif;
    }

    nav {
      display: flex;
      justify-content: space-between;
      padding: 0 24px;
      border-bottom: 1px solid var(--very-light-pink);
    }

    .menu {
      display: none;
    }

    .logo {
      width: 100px;
    }

    .navbar-left ul,
    .navbar-right ul {
      list-style: none;
      padding: 0;
      margin: 0;
      display: flex;
      align-items: center;
      height: 60px;
    }

    .navbar-left {
      display: flex;
    }

    .navbar-left ul {
      margin-left: 12px;
    }

    .navbar-left ul li a {
      text-decoration: none;
      color: var(--very-light-pink);
      border: 1px solid var(--white);
      padding: 8px;
      border-radius: 8px;
    }

    .navbar-left ul li a:hover,
    .navbar-right ul li a:hover {
      border: 1px solid var(--hospital-green);
      color: var(--hospital-green);
    }

    .navbar-email {
      color: var(--very-light-pink);
      font-size: var(--sm);
      margin-right: 12px;
    }

    .navbar-shopping-cart {
      position: relative;
    }

    .navbar-shopping-cart div {
      width: 16px;
      height: 16px;
      background-color: var(--hospital-green);
      font-size: var(--sm);
      font-weight: bold;
      position: absolute;
      top: -6px;
      right: -3px;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    @media (max-width: 640px) {
      .menu {
        display: block;
      }

      .navbar-left ul {
        display: none;;
      }

      .navbar-email {
        display: none;
      }
    }
   </style>
</head>
<body>
  <nav>
    <img src="./icons/icon_menu.svg" alt="menu" class="menu">

    <div class="navbar-left">
      <img src="./logos/logo_yard_sale.svg" alt="logo" class="logo">

      <ul>
        <li><a href="/">All</a></li>
        <li><a href="/">Clothes</a></li>
        <li><a href="/">Electronics</a></li>
        <li><a href="/">Furnitures</a></li>
        <li><a href="/">Toys</a></li>
        <li><a href="/">Others</a></li>
      </ul>
    </div>

    <div class="navbar-right">
      <ul>
        <li class="navbar-email">platzi@example.com</li>
        <li class="navbar-shopping-cart">
          <img src="./icons/icon_shopping_cart.svg" alt="shopping cart">
          <div>2</div>
        </li>
      </ul>
    </div>
  </nav>

</body>
</html>
```

## Detalle de producto

`html`

Les dejo mi propia versión de los puntitos :three:

```html
.points {
        display: flex;
        justify-content: center;
      }
      .points > li {
        background: var(--very-light-pink);
        width: 8px;
        height: 8px;
        margin: 8px 6px;
        border-radius: 50%;
        list-style-type: none;
        cursor: pointer;
      }
      .points > .active {
        background: var(--hospital-green);
      }
      .points > li:hover {
        background: var(--hospital-green);
      }
HTML

<div class="points">
        <li class="active"></li>
        <li></li>
        <li></li>
      </div>
```

`html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>

   <!-- fonts -->
   <link rel="preconnect" href="https://fonts.googleapis.com" />
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
   <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

   <style>
    :root {
      --very-light-pink: #c7c7c7;
      --text-input-field: #f7f7f7;
      --hospital-green: #acd992;
      --white: #ffffff;
      --black: #000000;
      --dark: #232330;
    
      /* fonts */
      --sm: 14px;
      --md: 16px;
      --lg: 18px;
    }
    
    body {
      margin: 0;
      font-family: 'Quicksand', sans-serif;
    }


    .product-detail {
      width: 360px;
      padding-bottom: 24px;
      position: absolute;
      right: 0;
    }
    
    .product-detail-close {
      background: var(--white);
      width: 14px;
      height: 14px;
      position: absolute;
      top: 24px;
      left: 24px;
      z-index: 2;
      padding: 12px;
      border-radius: 50%;
    }

    .product-detail-close:hover {
      cursor: pointer;
    }

    .product-detail > img:nth-child(2) {
      width: 360px;
      height: 360px;
      object-fit: cover;
      border-radius: 0 0 20px 20px;
    }

    .product-info {
      margin: 24px 24px 0 24px;
    }

    .product-info p:nth-child(1) {
      font-weight: bold;
      font-size: var(--md);
      margin-top: 0;
      margin-bottom: 4px;
    }

    .product-info p:nth-child(2) {
      color: var(--very-light-pink);
      font-weight: bold;
      font-size: var(--md);
      margin-top: 0;
      margin-bottom: 36px;
    }

    .product-info p:nth-child(3) {
      color: var(--very-light-pink);
      font-size: var(--sm);
      margin-top: 0;
      margin-bottom: 36px;
    }

    .primary-button {
      background-color: var(--hospital-green);
      border-radius: 8px;
      border: none;
      color: var(--white);
      width: 100%;
      cursor: pointer;
      font-size: var(--md);
      font-weight: bold;
      height: 50px;
    }

    .add-to-cart-button {
      display: flex;
      align-items: center;
      justify-content: center;
    }

    @media (max-width: 640px) {
      .product-detail {
        width: 100%;
      }
    }

   </style>
</head>
<body>
  <aside class="product-detail">
    <div class="product-detail-close">
      <img src="./icons/icon_close.png" alt="close">
    </div>
    <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="Bike">

    <div class="product-info">
      <p>135,00</p>
      <p>Bike</p>
      <p>With its practical position, this bike also  fulfills a decorative function, add your hall or workspace.</p>
      <button class="primary-button add-to-cart-button">
        <img src="./icons/bt_add_to_cart.svg" alt="add to cart">
        Add to cart
      </button>
    </div>
  </aside>
</body>
</html>
```

## Carrito de compras: HTML

`html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>

   <!-- fonts -->
   <link rel="preconnect" href="https://fonts.googleapis.com" />
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
   <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;500;700&display=swap" rel="stylesheet" />

   <style>
    :root {
      --white: #FFFFFF;
      --black: #000000;
      --very-light-pink: #C7C7C7;
      --text-input-field: #F7F7F7;
      --hospital-green: #ACD9B2;
      --sm: 14px;
      --md: 16px;
      --lg: 18px;
    }
    body {
      margin: 0;
      font-family: 'Quicksand', sans-serif;
    }
    .product-detail {
      width: 360px;
      padding: 24px;
      box-sizing: border-box;
      position: absolute;
      right: 0;
    }
    .title-container {
      display: flex;
    }
    .title-container img {
      transform: rotate(180deg);
      margin-right: 14px;
    }
    .title {
      font-size: var(--lg);
      font-weight: bold;
    }
    .order {
      display: grid;
      grid-template-columns: auto 1fr;
      gap: 16px;
      align-items: center;
      background-color: var(--text-input-field);
      margin-bottom: 24px;
      border-radius: 8px;
      padding: 0 24px;
    }
    .order p:nth-child(1) {
      display: flex;
      flex-direction: column;
    }
    .order p span:nth-child(1) {
      font-size: var(--md);
      font-weight: bold;
    }
    .order p:nth-child(2) {
      text-align: end;
      font-weight: bold;
    }
    .shopping-cart {
      display: grid;
      grid-template-columns: auto 1fr auto auto;
      gap: 16px;
      margin-bottom: 24px;
      align-items: center;
    }
    .shopping-cart figure {
      margin: 0;
    }
    .shopping-cart figure img {
      width: 70px;
      height: 70px;
      border-radius: 20px;
      object-fit: cover;
    }
    .shopping-cart p:nth-child(2) {
      color: var(--very-light-pink);
    }
    .shopping-cart p:nth-child(3) {
      font-size: var(--md);
      font-weight: bold;
    }
    .primary-button {
      background-color: var(--hospital-green);
      border-radius: 8px;
      border: none;
      color: var(--white);
      width: 100%;
      cursor: pointer;
      font-size: var(--md);
      font-weight: bold;
      height: 50px;
    }
    @media (max-width: 640px) {
      .product-detail {
        width: 100%;
      }
    }
   </style>
</head>
<body>
  <aside class="product-detail">
    <div class="title-container">
      <img src="./icons/flechita.svg" alt="arrow">
      <p class="title">My order</p>
    </div>

    <div class="my-order-content">
      <div class="shopping-cart">
        <figure>
          <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="bike">
        </figure>
        <p>Bike</p>
        <p>$30,00</p>
        <img src="./icons/icon_close.png" alt="close">
      </div>

      <div class="shopping-cart">
        <figure>
          <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="bike">
        </figure>
        <p>Bike</p>
        <p>$30,00</p>
        <img src="./icons/icon_close.png" alt="close">
      </div>

      <div class="shopping-cart">
        <figure>
          <img src="https://images.pexels.com/photos/276517/pexels-photo-276517.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" alt="bike">
        </figure>
        <p>Bike</p>
        <p>$30,00</p>
        <img src="./icons/icon_close.png" alt="close">
      </div>

      <div class="order">
        <p>
          <span>Total</span>
        </p>
        <p>$560.00</p>
      </div>

      <button class="primary-button">
        Checkout
      </button>
    </div>
  </div>
  </aside>
</body>
</html>
```



# 4. Próximos pasos

## Cómo continuar aprendiendo desarrollo

Nunca pares de Aprender!